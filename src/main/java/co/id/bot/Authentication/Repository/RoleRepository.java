/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.bot.Authentication.Repository;

import co.id.bot.Authentication.Model.UmRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author <Dhimas Surya G>
 */
@Repository
public interface RoleRepository extends JpaRepository<UmRole, Integer> {

    @Query(value = "select role_name FROM mrole where role_id= :roleId ",nativeQuery = true)
    String getRoleName(@Param("roleId") Integer roleId);
    
}
