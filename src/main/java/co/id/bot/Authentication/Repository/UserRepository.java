package co.id.bot.Authentication.Repository;

import co.id.bot.Authentication.Model.UserBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserRepository extends JpaRepository<UserBean, String> {

    UserBean findByEmail(String email);

    @Modifying
    @Query("UPDATE muser SET firstName = :firstName, lastName = :lastName, country = :country, image = :image, roleId= :roleId "
            + " WHERE email = :email")
    void saveWithoutPassword(
            @Param("firstName") String firstname,
            @Param("lastName") String lastname,
            @Param("country") String country,
            @Param("image") String image,
            @Param("email") String email,
            @Param("roleId") Integer roleId
    );

    @Query(value = "select count(*)from muser where email= :email ",nativeQuery=true)
    Integer checUserExist(@Param("email") String email);

    @Modifying
    @Query("UPDATE muser SET lang = :lang where email = :email")
    void updateLanguage(
            @Param("lang") String firstname,
            @Param("email") String email
    );
}
