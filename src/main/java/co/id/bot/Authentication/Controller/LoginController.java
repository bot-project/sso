package co.id.bot.Authentication.Controller;

import co.id.bot.Authentication.Configuration.Autologin;
import co.id.bot.Authentication.Model.UserBean;
import co.id.bot.Authentication.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@Controller
public class LoginController {

    @Autowired
    private Autologin autologin;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping(value = {"/", "/login"})
    public String login(Principal p) {
        if (p == null) {
            return "login";
        } else {
            return "redirect:/mainmenu";
        }

    }

    @GetMapping("/registration")
    public String showRegistration(UserBean userBean) {
        return "registration";
    }

    @PostMapping("/registration")
    public String registerUser(HttpServletResponse httpServletResponse, Model model, @Valid UserBean userBean, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userBean.setProvider("REGISTRATION");
        // Save the details in DB
        if (StringUtils.isNotEmpty(userBean.getPassword())) {
            userBean.setPassword(bCryptPasswordEncoder.encode(userBean.getPassword()));
        }
        //check email if already registered
        if (userRepository.checUserExist(userBean.getEmail()) == 1) {
            model.addAttribute("already", "* user / email sudah pernah digunakan");
            return "registration";
        } else {
            //Check domain email only use domain ascendcorp or bot
            if (CheckEmailDomain.getEmailDomain(userBean.getEmail())) {
                userBean.setRoleId(2);
                userBean.setLang("EN");
                userBean.setActive(0);
                userRepository.save(userBean);
                autologin.setSecuritycontext(userBean);
                model.addAttribute("loggedInUser", userBean);
                return "secure/user";
            } else {
                model.addAttribute("already", "* Wajib menggunakan email");
                return "registration";
            }
        }


    }

    /**
     * If we can't find a user/email combination
     */
    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }


    @RequestMapping("/exit")
    public String exit(HttpServletRequest request, HttpServletResponse response) {
        new SecurityContextLogoutHandler().logout(request, null, null);
        return "redirect:/login";
    }
}
