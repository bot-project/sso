package co.id.bot.Authentication.Controller;

import co.id.bot.Authentication.Model.UserBean;
import co.id.bot.Authentication.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api")
public class LanguageController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping(value = "lang")
    public String getLang(String username){
        UserBean bean = userRepository.findByEmail(username);
        return bean.getLang();
    }

    public void updateLang(String username, String lang){
        userRepository.updateLanguage(username, lang);
    }
}
