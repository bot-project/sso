package co.id.bot.Authentication.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class DashboardController {

    @GetMapping(value = "mainmenu")
    public String dashboard(){
        return "dashboard";
    }
}
