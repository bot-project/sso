package co.id.bot.Authentication.Configuration;

import java.util.HashSet;
import java.util.Set;

import co.id.bot.Authentication.Model.UserBean;
import co.id.bot.Authentication.Repository.RoleRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class Autologin {

    @Autowired
    private RoleRepository roleRepository;

    public void setSecuritycontext(UserBean userForm) {
        String roleBean = roleRepository.getRoleName(userForm.getRoleId());
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(roleBean));
        Authentication authentication = new UsernamePasswordAuthenticationToken(userForm.getEmail(), userForm.getPassword(), grantedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);

    }
}