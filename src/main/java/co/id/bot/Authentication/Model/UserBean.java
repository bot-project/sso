package co.id.bot.Authentication.Model;

import lombok.Data;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Entity(name = "muser")
@Table(name = "muser")
public class UserBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "Email  cannot be empty")
    @Email(message = "Email Format is not valid")
    @Size(min = 3, max = 30, message = "Email can not be empty")
    @Id
    private String email;

    @NotNull(message = "First Name cannot be empty")
    @Size(min = 3, max = 30, message = "First Name cannot be less than 3 characters")
    @Column(name = "first_name")
    private String firstName;

    @NotNull(message = "Last Name cannot be empty")
    @Size(min = 3, max = 30, message = "Last Name cannot be less than 3 characters")
    @Column(name = "last_name")
    private String lastName;

    private String title;
    private String country;
    private String password;
    @Column(name = "role_id")
    private Integer roleId;
    private Integer active;

    @Transient
    private String passwordConfirm;
    private String provider;
    private String image;
    private String lang;


}
