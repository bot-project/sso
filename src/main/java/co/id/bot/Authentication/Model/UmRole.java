/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.id.bot.Authentication.Model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author <Dhimas Surya G>
 */
@Data
@Entity(name = "mrole")
@Table(name = "mrole")
public class UmRole implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId;
    private String roleName;
    private String roleCode;

    public UmRole(Integer roleId, String roleName, String roleCode) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.roleCode = roleCode;
    }

    public UmRole() {
    }

}
